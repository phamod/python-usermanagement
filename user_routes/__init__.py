"""
Module for handling user management related actions like logging in
"""
from flask import Flask, Blueprint, request, json
from usermanager import UserManager
from application import APP, BP, TOKEN_MANAGER


USER_MANAGER = UserManager(
    'ldap://172.17.0.1:389', 'cn=hamod,ou=users,dc=example,dc=com', 'password',
    config={'user_path':"ou=users,dc=example,dc=com", 'roles_path':"ou=roles,dc=example,dc=com"})

CREDENTIALS_NOT_PROVIDED_MESSAGE = "User and password were not provided to login server."
CREDENTIALS_INVALID_MESSAGE = "your username and password are not correct."

@BP.route("/login", methods=['POST'])
def login():
    """Route for hadling a user logging in.

    Args:
      username (str): http body value for username
      password (str): http body value for user's password
    Return:
      Response (http response): response containing body token or error message
    """
    response = None
    user = None
    pwd = None
    data = {}

    if request.is_json:
        user = request.get_json().get('username')
        pwd = request.get_json().get('password')

    if user is not None and pwd is not None:
        logged_in, distinct_name = USER_MANAGER.login(user, pwd)
        if logged_in:
            roles = USER_MANAGER.get_roles(distinct_name)
            token = TOKEN_MANAGER.generate_refresh_token(distinct_name, roles)
            data['token'] = token.decode("utf-8")
            response = APP.response_class(response=json.dumps(data), status=200)
        else:
            response = APP.response_class(
                response=CREDENTIALS_INVALID_MESSAGE, status=400)
    else:
        response = APP.response_class(
            response=CREDENTIALS_NOT_PROVIDED_MESSAGE,
            status=400)

    return response

@BP.route("/hello", methods=["POST"])
@TOKEN_MANAGER.check_permission(role="user")
def helloworld():
    """Endpoint for testing logged in security checks."""
    return "hello world"


APP.register_blueprint(BP, url_prefix='/v1')
