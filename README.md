[![pipeline status](https://gitlab.com/phamod/python-usermanagement/badges/master/pipeline.svg)](https://gitlab.com/phamod/python-usermanagement/-/commits/master)
[![coverage report](https://gitlab.com/phamod/python-usermanagement/badges/master/coverage.svg)](https://gitlab.com/phamod/python-usermanagement/-/commits/master)

docker run --net host --entrypoint bash -e SONAR_LOGIN=admin -e SONAR_PASSWORD=admin -e SONAR_HOST_URL=http://192.168.2.63:9000 --user="root" -it -v "$pwd:/usr/src" sonarsource/sonar-scanner-cli

## Running tests
> coverage run -m pytest
> coverage html

## running test server
> python app.py

## running pylint
> pylint usermanager tokenmanager query_routes user_routes setup.py