"""
script for handling the development server
"""
from flask_socketio import SocketIO
import user_routes  # pylint: disable=unused-import
import query_routes  # pylint: disable=unused-import
import camera_route
from application import APP


SOCKET_APP = SocketIO(APP, cors_allowed_origins="*")

@SOCKET_APP.on('my event')
def handle_my_custom_event(json):
    """socket handler for receiving from client.

    Args:
        json (dict): json object received
    """
    print('received json: ' + str(json))


# end video stream setup

if __name__ == "__main__":
    from flask_cors import CORS
    CORS(APP)

    SOCKET_APP.run(APP)

    # release the video stream pointer
    camera_route.Test.VS.stop()
