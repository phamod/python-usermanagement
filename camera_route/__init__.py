"""
Module for handling routes to a camera feed
"""
import threading
import argparse
import datetime
import time
from flask import Flask, Blueprint, request, json, Response, render_template
from SPARQLWrapper import SPARQLWrapper, JSON
from imutils.video import VideoStream
import imutils
import cv2
from application import APP, BP



class Test():
    """class to handle a camera stream."""
    # initialize the output frame and a LOCK used to ensure thread-safe
    # exchanges of the output frames (useful for multiple browsers/tabs
    # are viewing tthe stream)
    OUTPUT_FRAME = None
    LOCK = threading.Lock()


    # initialize the video stream and allow the camera sensor to
    # warmup
    #VS = VideoStream(usePiCamera=1).start()
    VS = VideoStream(src=0).start()
    time.sleep(2.0)

    @classmethod
    def detect_motion(cls):
        """Function for grabbing the next frame of the camera to stream."""

        # loop over frames from the video stream
        while True:
            # read the next frame from the video stream, resize it,
            # convert the frame to grayscale, and blur it
            frame = cls.VS.read()
            frame = imutils.resize(frame)

            # grab the current timestamp and draw it on the frame
            timestamp = datetime.datetime.now()
            cv2.putText(frame, timestamp.strftime( # pylint: disable=no-member
                "%A %d %B %Y %I:%M:%S%p"), (10, frame.shape[0] - 10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1) # pylint: disable=no-member

            # acquire the LOCK, set the output frame, and release the
            # LOCK
            with cls.LOCK:
                cls.OUTPUT_FRAME = frame.copy()

    @classmethod
    def generate(cls):
        """Generates a new camera image to send to the client."""
        # loop over frames from the output stream
        while True:
            # wait until the LOCK is acquired
            with cls.LOCK:
                # check if the output frame is available, otherwise skip
                # the iteration of the loop
                if cls.OUTPUT_FRAME is None:
                    continue

                # encode the frame in JPEG format
                (flag, encoded_image) = cv2.imencode(".jpg", cls.OUTPUT_FRAME) # pylint: disable=no-member

                # ensure the frame was successfully encoded
                if not flag:
                    continue

            # yield the output frame in the byte format
            yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
                  bytearray(encoded_image) + b'\r\n')


@BP.route("/video_feed")
def video_feed():
    """Creates endpoint to get video stream."""
    # return the response generated along with the specific media
    # type (mime type)
    return Response(Test.generate(),
                    mimetype="multipart/x-mixed-replace; boundary=frame")


# start a thread that will perform motion detection
THREAD = threading.Thread(target=Test.detect_motion)
THREAD.daemon = True
THREAD.start()

APP.register_blueprint(BP, url_prefix='/v1')
