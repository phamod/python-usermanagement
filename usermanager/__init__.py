"""
Module used to create a class to be used to handle ldap operations. ldap can be used to create
users and login and handle user management.
"""
import re
import ldap

class UserManager:
    """
    Class to handle usermanagement including logging in, creating users and deleting a user

    Args:
        url (str): url to the ldap server used for containing the user data
        user (str): application user used to connect and handle connection the ldap server
        pwd (str): password passed to handle connection to the ldap server
        user_path (str): ldap path to directory containing user
        roles_path (str): ldap path to directory containing roles
    """
    ID_LENGTH = 10
    def __init__(self, url, user, pwd,
                 config=None):
        self.url = url
        self.user = user
        self.pwd = pwd
        self.config = config
        print(self.config)

    @staticmethod
    def parse_id_from_dn(distinct_name):
        """parse dod id from an ldap distinct name.

        Args:
            distinct_name (str): ldap distinct name to parse dod id from
        Returns:
            dod_id (str): 10 digit dod number or None if not found
        """
        dod_id = None
        #common name is the first listed in the distinct name seperated by comma
        common_name = distinct_name.split(',')[0]
        #the common name is firstname.middleInitial.lastname.idNumber
        match = re.search(r'\.\d{10}$', common_name)
        if match is not None:
            dod_id = match.group(0)
            dod_id = dod_id.replace('.', '')
        return dod_id

    @staticmethod
    def get_role_from_dn(role_dn):
        """Finds the name of the role give a distinct name.

        Args:
            role_dn (str): distinct name of the role
        Returns:
            role (str): the role without the ldap diretory
        """
        role = role_dn.split(",")[0]
        role_cn_pair = role.split("=")
        if len(role_cn_pair) > 1:
            role = role_cn_pair[1]
        return role

    def get_user_dn_from_id(self, dod_id):
        """Uses dod id to find the user associated with the given smart card.

        Args:
            dod_id (str): 10 digit dod id number
        Returns:
            distinct_name (str): distinct name of the user associated with the given id
        """
        distinct_name = None
        try:
            connection = ldap.initialize(self.url)
            connection.simple_bind_s(self.user, self.pwd)
            result = connection.search_s(self.config['roles_path'],
                                         ldap.SCOPE_SUBTREE, 'uid=%s' % dod_id) # pylint: disable=no-member
            if len(result) > 1:
                print("too many users have this id")
            elif len(result) == 1:
                # the first element of the tuple in the result list is the distinct name
                # the second element is a dictionary containing a list of allatribute pairs
                distinct_name = result[0][0]
        finally:
            del connection
        return distinct_name

    def login(self, user, pwd):
        """Attempts to login a user into ldap and returns a boolean of access.

        Args:
            user (str): user to be logged in as
            pwd (str): password using to login with the user
        Returns:
            logged_in (bool): status of whether the user loggged in or not
        """
        logged_in = False
        distinct_name = "cn=%s,%s" % (user, self.config['user_path'])
        try:
            connection = ldap.initialize(self.url)
            connection.simple_bind_s(distinct_name, pwd)
            logged_in = True
        # Disable check of name in module because ldap use of .so file seems not to work with
        # ldap error
        # pylint: disable=no-member
        except ldap.LDAPError as error:
            print(error)
            logged_in = False
        finally:
            del connection

        return logged_in, distinct_name

    def get_roles(self, user_dn):
        """Given a user distinct name find the list of roles associated with it.

        Args:
            user_dn (str): distinct name of user to find roles for
        Returns:
            roles (list): string list of roles associated with the user
        """
        roles = ["user"]
        try:
            connection = ldap.initialize(self.url)
            connection.simple_bind_s(self.user, self.pwd)
            result = connection.search_s('ou=roles,dc=example,dc=com',
                                         ldap.SCOPE_SUBTREE, 'member=%s' % user_dn) # pylint: disable=no-member
            for role in result:
                roles.append(UserManager.get_role_from_dn(role[0]))
        finally:
            del connection
        return roles
