"""
script for handling the container server
"""
from flask import Flask, Blueprint, request, json
from tokenmanager import TokenManager

KEY = 'secret'

APP = Flask(__name__)
BP = Blueprint('main', __name__)
TOKEN_MANAGER = TokenManager(KEY)
