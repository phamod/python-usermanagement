"""
Module for generating build of these libraries for pip to install
"""
import os
import setuptools

LIB_FOLDER = os.path.dirname(os.path.realpath(__file__))
REQUIREMENTS_PATH = LIB_FOLDER + '/requirements.txt'
INSTALL_REQUIRES = [] # Examples: ["gunicorn", "docutils>=0.3", "lxml==0.5a7"]
if os.path.isfile(REQUIREMENTS_PATH):
    with open(REQUIREMENTS_PATH) as f:
        INSTALL_REQUIRES = f.read().splitlines()

print(INSTALL_REQUIRES)

with open("README.md", "r") as fh:
    LONG_DESCRIPTION = fh.read()

setuptools.setup(
    name="userManagement", # Replace with your own username
    version="0.0.1",
    author="Example Author",
    author_email="author@example.com",
    description="A small example package",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Scripting Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=INSTALL_REQUIRES
)
