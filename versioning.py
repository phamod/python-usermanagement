import re
import sys

def update_version(path):
    output=""

    with open (path, "r") as myfile:
        data=myfile.readlines()
        for line in data:
            if line.find("version") > -1:
                matchObj = re.search(r"[0-9.]+", line)
                oldversion=matchObj.group()
                matchObj=re.search(r"[0-9]+$", oldversion)
                minorversion = int(matchObj.group())
                version = oldversion.replace(str(minorversion), str(minorversion + 1))
                output += line.replace(oldversion, version)
            else:
                output += line

    with open("setup.py", "w") as outfile:
        outfile.write(output)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("usage: python3 versioning.py <input file>")
    else:
        update_version(sys.argv[1])