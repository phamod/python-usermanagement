#pylint: disable=missing-function-docstring
#pylint: disable=missing-module-docstring
#pylint: disable=missing-class-docstring
#pylint: disable=unused-argument
#pylint: disable=no-self-use
#pylint: disable=too-few-public-methods

import pytest
import ldap
from usermanager import UserManager

DISTINCT_NAME = "cn=hamod,ou=users,dc=example,dc=com"
LDAP_URL = "ldap://localhost:389"
CONFIG = {'user_path':"ou=users,dc=example,dc=com", 'roles_path':"ou=roles,dc=example,dc=com"}

@pytest.fixture()
def mock_ldap(monkeypatch):
    class MockLdapObject:
        def simple_bind_s(self, user, pwd):
            # this method is just a mock of what ldap connecting
            pass

        def search_s(self, *args, **kwargs):
            return []

    def mock_initialize(*args, **kwargs):
        return MockLdapObject()
    monkeypatch.setattr(ldap, "initialize", mock_initialize)

@pytest.fixture()
def mock_ldap1return(monkeypatch):
    class MockLdapObject:
        def simple_bind_s(self, user, pwd):
            # this method is just a mock of what ldap connecting
            pass

        def search_s(self, *args, **kwargs):
            return [(DISTINCT_NAME, {})]

    def mock_initialize(*args, **kwargs):
        return MockLdapObject()
    monkeypatch.setattr(ldap, "initialize", mock_initialize)

@pytest.fixture()
def mock_ldap2return(monkeypatch):
    class MockLdapObject:
        def simple_bind_s(self, user, pwd):
            # this method is just a mock of what ldap connecting
            pass

        def search_s(self, *args, **kwargs):
            return [(DISTINCT_NAME, {}), ("", {})]

    def mock_initialize(*args, **kwargs):
        return MockLdapObject()
    monkeypatch.setattr(ldap, "initialize", mock_initialize)

@pytest.fixture()
def mock_ldap_login(monkeypatch):
    class MockLdapObject:
        def simple_bind_s(self, user, pwd):
            if user != 'cn=valid,ou=users,dc=example,dc=com':
                raise ldap.LDAPError("bind failed")

    def mock_initialize(*args, **kwargs):
        return MockLdapObject()
    monkeypatch.setattr(ldap, "initialize", mock_initialize)

def test_constructor_sets_url_for_connection(mock_ldap):
    expected = LDAP_URL
    user_manager = UserManager(expected, 'test', 'test')
    assert user_manager.url == expected

def test_parse_id_from_dn_with_good_cert():
    #it's expected that the id is a 10 digit number attached to the common name
    expected = "0000000000"
    #pylint: disable=line-too-long
    distinct_name = "CN=HAMROD.PATRICK.FITZPATRICK.0000000000,OU=USN,OU=PKI,OU=DoD,O=U.S. Government,C=US"
    id = UserManager.parse_id_from_dn(distinct_name)
    assert id == expected

def test_parse_id_from_dn_with_bad_cert_id_small():
    #it's expected that the id is a 10 digit number attached to the common name
    #this however is 9 digit
    expected = None
    #pylint: disable=line-too-long
    distinct_name = "CN=HAMROD.PATRICK.FITZPATRICK.NotAnId999999999,OU=USN,OU=PKI,OU=DoD,O=U.S. Government,C=US"
    id = UserManager.parse_id_from_dn(distinct_name)
    assert id == expected

def test_parse_id_from_dn_with_bad_cert_id_large():
    #it's expected that the id is a 10 digit number attached to the common name
    #this however is 11 digit
    expected = None
    #pylint: disable=line-too-long
    distinct_name = "CN=HAMROD.PATRICK.FITZPATRICK.NotAnId33333333333,OU=USN,OU=PKI,OU=DoD,O=U.S. Government,C=US"
    id = UserManager.parse_id_from_dn(distinct_name)
    assert id == expected

def test_parse_id_from_dn_with_no_cert_id():
    #it's expected that the id is a 10 digit number attached to the common name
    #this however is 11 digit
    expected = None
    #pylint: disable=line-too-long
    distinct_name = "CN=HAMROD.PATRICK.FITZPATRICK.NotAnId,OU=USN,OU=PKI,OU=DoD,O=U.S. Government,C=US"
    id = UserManager.parse_id_from_dn(distinct_name)
    assert id == expected

def test_get_user_dn_from_id_with_good_id(mock_ldap1return):
    user_manager = UserManager(LDAP_URL, 'test', 'test', config=CONFIG)
    result = user_manager.get_user_dn_from_id("0000000000")
    assert result == DISTINCT_NAME

def test_get_user_dn_from_id_with_unfound_id(mock_ldap):
    user_manager = UserManager(LDAP_URL, 'test', 'test', config=CONFIG)
    result = user_manager.get_user_dn_from_id("0000000000")
    assert result == None

def test_get_user_dn_from_id_with_too_many_ids_returned(mock_ldap2return):
    user_manager = UserManager(LDAP_URL, 'test', 'test', config=CONFIG)
    result = user_manager.get_user_dn_from_id("0000000000")
    assert result == None

def test_get_role_from_dn_with_good_dn():
    result = UserManager.get_role_from_dn("cn=champion,ou=roles,dc=example,dc=com")
    assert result == "champion"

def test_get_role_from_dn_with_bad_input():
    result = UserManager.get_role_from_dn("admin")
    assert result == "admin"

def test_get_role_from_dn_with_bad_cn():
    result = UserManager.get_role_from_dn("root,ou=roles,dc=example,dc=com")
    assert result == "root"

def test_get_roles_with_one_search_result(monkeypatch):
    class MockLdapObject:
        def simple_bind_s(self, user, pwd):
            # this method is just a mock of what ldap connecting
            pass

        def search_s(self, *args, **kwargs):
            return [("cn=admin,ou=roles,dc=example,dc=com", {})]

    def mock_initialize(*args, **kwargs):
        return MockLdapObject()
    monkeypatch.setattr(ldap, "initialize", mock_initialize)
    user_manager = UserManager(LDAP_URL, 'test', 'test')
    result = user_manager.get_roles(DISTINCT_NAME)
    assert result == ["user", "admin"]

def test_get_roles_with_no_search_result(mock_ldap):
    user_manager = UserManager(LDAP_URL, 'test', 'test')
    result = user_manager.get_roles(DISTINCT_NAME)
    assert result == ["user"]

def test_get_roles_with_multiple_search_result(monkeypatch):
    class MockLdapObject:
        def simple_bind_s(self, user, pwd):
            # this method is just a mock of what ldap connecting
            pass

        def search_s(self, *args, **kwargs):
            return [("cn=admin,ou=roles,dc=example,dc=com", {}),
                    ("cn=helmsman,ou=roles,dc=example,dc=com", {})]

    def mock_initialize(*args, **kwargs):
        return MockLdapObject()
    monkeypatch.setattr(ldap, "initialize", mock_initialize)
    user_manager = UserManager(LDAP_URL, 'test', 'test')
    result = user_manager.get_roles(DISTINCT_NAME)
    assert result == ["user", "admin", "helmsman"]

def test_login_with_bad_credentials(mock_ldap_login):
    user_manager = UserManager(LDAP_URL, 'test', 'test', config=CONFIG)
    logged_in, _ = user_manager.login("test", 'test')
    assert False == logged_in

def test_login_with_good_credentials(mock_ldap_login):
    user_manager = UserManager(LDAP_URL, 'test', 'test', config=CONFIG)
    logged_in, distinct_name = user_manager.login("valid", 'test')
    assert distinct_name == 'cn=valid,ou=users,dc=example,dc=com'
    assert True == logged_in
