from application import APP, TOKEN_MANAGER
import user_routes
#pylint: disable=missing-function-docstring
#pylint: disable=missing-module-docstring
import tokenmanager
import pytest

BAD_TOKEN = TOKEN_MANAGER.generate_refresh_token(user="patrick", roles=[])
GOOD_TOKEN = TOKEN_MANAGER.generate_refresh_token(user="patrick", roles=['user'])

HELLO_ROUTE = '/v1/hello'
LOGIN_ROUTE = '/v1/login'

@pytest.fixture()
def mock_usermanager(monkeypatch):
    class MockUsermanager:
        def login(self, user, pwd, **kwargs):
            return user == 'valid', None

        def get_roles(self, *args, **kwargs):
            return ['user']

    monkeypatch.setattr(user_routes, "USER_MANAGER", MockUsermanager())

def test_hello_route_with_bad_token():
    with APP.test_client() as client:
        rv = client.post(HELLO_ROUTE)
        data = rv.get_data().decode('utf-8')
        assert data == tokenmanager.BAD_TOKEN_MESSAGE
        assert rv.status_code == 200

def test_hello_route_without_role():
    with APP.test_client() as client:
        rv = client.post(HELLO_ROUTE,
            headers={'token': BAD_TOKEN})
        data = rv.get_data().decode('utf-8')
        assert data == tokenmanager.UNAUTHORIZED_TOKEN % 'user'
        assert rv.status_code == 200

def test_hello_route_with_token():
    with APP.test_client() as client:
        rv = client.post(HELLO_ROUTE,
            headers={'token': GOOD_TOKEN})
        data = rv.get_data().decode('utf-8')
        assert data == 'hello world'
        assert rv.status_code == 200

def test_login_route_without_username_and_password():
    with APP.test_client() as client:
        rv = client.post(LOGIN_ROUTE)
        data = rv.get_data().decode('utf-8')
        assert data == user_routes.CREDENTIALS_NOT_PROVIDED_MESSAGE
        assert rv.status_code == 400

def test_login_route_without_password():
    with APP.test_client() as client:
        rv = client.post(LOGIN_ROUTE, json={'username':'patrick'})
        data = rv.get_data().decode('utf-8')
        assert data == user_routes.CREDENTIALS_NOT_PROVIDED_MESSAGE
        assert rv.status_code == 400

def test_login_route_without_username():
    with APP.test_client() as client:
        rv = client.post(LOGIN_ROUTE, json={'password':'patrick'})
        data = rv.get_data().decode('utf-8')
        assert data == user_routes.CREDENTIALS_NOT_PROVIDED_MESSAGE
        assert rv.status_code == 400

def test_login_route_with_invalid_credentials(mock_usermanager):
    with APP.test_client() as client:
        rv = client.post(LOGIN_ROUTE, json={'password':'patrick', 'username': 'patrick'})
        data = rv.get_data().decode('utf-8')
        assert data == user_routes.CREDENTIALS_INVALID_MESSAGE
        assert rv.status_code == 400

def test_login_route_with_good_credentials(mock_usermanager):
    with APP.test_client() as client:
        rv = client.post(LOGIN_ROUTE, json={'password':'password', 'username': 'valid'})
        data = rv.get_data().decode('utf-8')
        assert TOKEN_MANAGER.is_authorized_access(data, 'user')
        assert rv.status_code == 200