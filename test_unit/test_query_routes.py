#pylint: disable=missing-function-docstring
#pylint: disable=missing-module-docstring
from query_routes import querymanager

def test_get_bindings_with_results():
    expected = ['s', 'p', 'o']
    actual = querymanager.get_bindings("""select * {
        ?s ?p ?o
    }
    """)
    assert expected == actual

def test_get_bindings_will_have_no_duplicates():
    expected = ['s', 'p', 'o']
    actual = querymanager.get_bindings("""select ?s { ?s ?p ?o }""")
    assert expected == actual

def test_get_bindings_with_no_results():
    expected = []
    actual = querymanager.get_bindings("")
    assert expected == actual

def test_generate_headers_with_bindings():
    expected = [{'title': 's', 'field': 's'}, {'title': 'p', 'field': 'p'}]
    actual = querymanager.generate_headers(['s', 'p'])
    assert expected == actual

def test_generate_headers_with_no_bindings():
    expected = []
    actual = querymanager.generate_headers([])
    assert expected == actual

def test_generate_tabulator_rows_with_good_return():
    expected = [{'id': 0, 's': 'patrick', 'p': 'a', 'o': 'man'}]
    query_result = {'head': {'vars': ['s', 'p', 'o']},
                    'results': {'bindings' : [
                        {'s':{'type': 'uri', 'value':'patrick'},
                         'p':{'type':'uri', 'value':'a'},
                         'o':{'type': 'string', 'value':'man'}}]}}

    actual = querymanager.generate_tabulator_rows(query_result)
    assert actual == expected

def test_generate_tabulator_rows_with_no_bound_result():
    expected = [{'id': 0}]
    query_result = {'head': {'vars': ['s', 'p', 'o']},
                    'results': {'bindings' : [{}]}}

    actual = querymanager.generate_tabulator_rows(query_result)
    assert actual == expected
