#pylint: disable=missing-function-docstring
#pylint: disable=missing-module-docstring
import datetime
import jwt
import pytest
from tokenmanager import TokenManager
import tokenmanager

KEY = 'secret'

@pytest.fixture(name="expired_token")
def generate_expired_token():
    payload = {'user': 'fox', 'roles': ['users', 'starfox'], 'exp':0}
    token = jwt.encode(payload, KEY, algorithm='HS256')
    return token

def test_generate_refresh_token_creates_token_with_given_user_and_roles_with_an_expiration_time():
    token_manager = TokenManager(KEY)
    expiration = (datetime.datetime.now() +
                  datetime.timedelta(seconds=token_manager.refresh_expiration)).timestamp()
    expiration = int(expiration)
    user = 'metalgear'
    roles = ['users', 'greyfox']
    expected = {'exp': expiration, 'roles': roles, 'user': user}
    token = token_manager.generate_refresh_token(user, roles)
    payload = jwt.decode(token, KEY, algorithms=[token_manager.algorithm])
    assert payload == expected

def test_is_authorized_access_with_expired_token_should_return_false_reason(expired_token):
    token_manager = TokenManager(KEY)
    is_authorized, reason = token_manager.is_authorized_access(expired_token, 'user')
    assert not is_authorized
    assert reason == tokenmanager.EXPIRED_TOKEN_MESSAGE

def test_is_authorized_access_without_needed_to_role():
    token_manager = TokenManager(KEY)
    token = token_manager.generate_refresh_token('user', ['user'])
    is_authorized, reason = token_manager.is_authorized_access(token, 'admin')
    assert not is_authorized
    assert reason == tokenmanager.UNAUTHORIZED_TOKEN % 'admin'

def test_is_authorized_access_with_needed_to_role():
    token_manager = TokenManager(KEY)
    token = token_manager.generate_refresh_token('user', ['user'])
    is_authorized, reason = token_manager.is_authorized_access(token, 'user')
    assert is_authorized
    assert reason == ""

def test_is_authorized_access_with_bad_token():
    token_manager = TokenManager(KEY)
    is_authorized, reason = token_manager.is_authorized_access("", 'user')
    assert not is_authorized
    assert reason == tokenmanager.BAD_TOKEN_MESSAGE
 