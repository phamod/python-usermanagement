"""
Module used for helper functions to create queries and return results for tabulator
"""
import re


def get_bindings(query_string):
    """parse the list of sparql bindings to be used for display.

    Args:
        query_string (str): sparql query to find bindings in
    Returns:
        bindings (list): list of bindings
    """
    bindings = []
    findings = re.findall('[?][a-zA-Z0-9]*', query_string)
    for finding in findings:
        binding = finding.replace("?", "")
        if binding not in bindings:
            bindings.append(binding)
    return bindings

def generate_headers(bindings):
    """generate column headers to be used for tabulator-tables js.

    Args:
        bindings (list): sparql query to find bindings in
    Returns:
        columns (list): list of column definitions for tabulator-tables
    """
    columns = []
    for binding in bindings:
        column = {'title': binding, 'field': binding}
        columns.append(column)
    return columns

def generate_tabulator_rows(result_json):
    """generate row definitions to be used for tabulator-tables js.

    Args:
        result_json (str): sparql_wrapper query return defitions
    Returns:
        rows (list): list of row definitions for tabulator-tables
    """
    rows = []
    bindings = result_json['head']['vars']
    results = result_json['results']['bindings']
    identity = 0
    for result in results:
        row = {'id': identity}
        for binding in bindings:
            try:
                row[binding] = result[binding]['value']
            except KeyError:
                pass
        identity += 1
        rows.append(row)
    return rows
