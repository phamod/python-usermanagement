"""
Module for handling routes to query both sql and sparql data sources
"""
from flask import Flask, Blueprint, request, json
from SPARQLWrapper import SPARQLWrapper, JSON
from application import APP, BP, TOKEN_MANAGER
from . import querymanager


@BP.route("/query/sparql", methods=["POST"])
@TOKEN_MANAGER.check_permission(role="user")
def query():
    """Route for handling a sqparql query and returning a tabulaor data list.

    Args:
        query (str): http body value used to query the sparql endpoint
    Return:
        ret (list): tabulator-tables list of data to fill table
    """
    ret = []
    query_string = request.get_json().get("query")
    sparql = SPARQLWrapper("http://localhost:5820/test/query")
    sparql.setCredentials("admin", "admin")
    sparql.addParameter("reasoning", "true")
    sparql.setReturnFormat(JSON)
    sparql.setQuery(query_string)
    ret = sparql.query().convert()
    ret = querymanager.generate_tabulator_rows(ret)
    return APP.response_class(response=json.dumps(ret), status=200)

@BP.route("/query/sparql_headers", methods=["POST"])
@TOKEN_MANAGER.check_permission(role="user")
def query_headers():
    """Route for handling a sqparql query and returning a tabulaor column header list.

    Args:
        query (str): http body value used to query the sparql endpoint
    Return:
        ret (list): tabulator-tables list of column headers to fill table
    """
    query_string = request.get_json().get("query")
    bindings = querymanager.get_bindings(query_string)
    columns = querymanager.generate_headers(bindings)
    return APP.response_class(response=json.dumps(columns), status=200)

APP.register_blueprint(BP, url_prefix='/v1')
