"""
Module for handling management of tokens to authorize and authenticate users
"""
import datetime
import time
from functools import wraps
import jwt
from flask import request

BAD_TOKEN_MESSAGE = "Bad token"
EXPIRED_TOKEN_MESSAGE = "Token has expired"
UNAUTHORIZED_TOKEN = "Unauthorized user doesn't have the following role: %s"


class TokenManager:
    """
    Class to handle the management of the tokens that authenticate and authorize users

    Args:
        key (str): key used to generate and validate the token
        algorithm (str): jwt algoritm used to generate the token
        ref_expiration (int): time seconds that the token is valid
    """
    def __init__(self, key, algorithm='HS256', ref_expiration=86400):
        self.key = key
        self.algorithm = algorithm
        self.refresh_expiration = ref_expiration

    def generate_refresh_token(self, user, roles):
        """Generate a new token containing the user and their roles.

        Args:
            user (str): user the token is being generated for
            roles (list): list of roles to be added to the token
        Returns:
            encoded jwt token
        """
        payload = {'user': user,
                   'roles':roles,
                   'exp': datetime.datetime.utcnow()
                          + datetime.timedelta(seconds=self.refresh_expiration)}
        return jwt.encode(payload, self.key, algorithm=self.algorithm)

    def is_authorized_access(self, token, role):
        """Checks if the given token has the role to access a certain function.

        Args:
            token (jwt encoded token): token to be decoded
            role (str): role being checked against token for access
        """
        is_authorized = False
        reason = UNAUTHORIZED_TOKEN % role
        try:
            payload = jwt.decode(token, self.key, algorithms=[self.algorithm])
            roles = payload.get("roles")
            for token_role in roles:
                if token_role == role:
                    is_authorized = True
                    reason = ""
                    break
        except jwt.ExpiredSignatureError:
            reason = EXPIRED_TOKEN_MESSAGE
        except jwt.InvalidTokenError:
            reason = BAD_TOKEN_MESSAGE

        return is_authorized, reason

    def check_permission(self, role="user"):
        """Decorator for checking user has appropriate role.

        Using this function will be added above the used method @check_permission() or
        @check_permission(role=)

        Args:
            function (func): functions to add decoration
        Return:
            (func): function to run when the input function is called
        """
        def wrapper(function):
            @wraps(function)
            def decorator(*args, **kwargs):
                token = request.headers.get('token')
                authorized, message = self.is_authorized_access(token, role)
                ret = None
                if authorized:
                    ret = function(*args, **kwargs)
                else:
                    ret = message
                return ret
            return decorator
        return wrapper
