FROM python:latest

WORKDIR /usr/src/app

COPY . .

RUN bash -c 'apt-get update'
RUN bash -c 'apt-get install -y libldap2-dev libsasl2-dev'
RUN pip3 install -r requirements.txt
RUN pip3 install -r testRequire.txt
RUN pylint usermanager tokenmanager query_routes user_routes setup.py
#RUN coverage run -m pytest



